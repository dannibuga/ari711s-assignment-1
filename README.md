# ARI711S Assignment 1

By: Edilson Zau <br>
Student #: 220090491

## Problem 1
In this problem, the task is to build a linear regression model using a multi-layer perceptron (MLP) and the Ridge regularisation in the Julia programming language. 

## Problem 2
In this problem, the task is to build an image classifier that identifies pneumonia and normal x-ray photos, using the flux package and the Julia programming language.
